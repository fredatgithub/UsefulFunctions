﻿/*
The MIT License(MIT)
Copyright(c) 2015 Freddy Juhel
Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:
The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.
THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
*/
using System;
namespace FonctionsUtiles.Fred.Csharp
{
  public static class CommonTypes
  {
    public static readonly Type TypeOfBoolean = typeof(bool);
    public static readonly Type TypeOfBool = typeof(bool);
    public static readonly Type TypeOfString = typeof(string);
    public static readonly Type TypeOfShort = typeof (short);
    public static readonly Type TypeOfInt16 = typeof(short);
    public static readonly Type TypeOfInt32 = typeof(int);
    public static readonly Type TypeOfInt = typeof(int);
    public static readonly Type TypeOfInt64 = typeof(long);
    public static readonly Type TypeOfLong = typeof(long);
    public static readonly Type TypeOfUlong = typeof(ulong);
    public static readonly Type TypeOfUInt64 = typeof(ulong);
    public static readonly Type TypeOfUint = typeof(uint);
    public static readonly Type TypeOfUshort = typeof(ushort);
    public static readonly Type TypeOfByte = typeof(byte);
    public static readonly Type TypeOfChar = typeof(char);
    public static readonly Type TypeOfDecimal = typeof(decimal);
    public static readonly Type TypeOfdouble = typeof(double);
    public static readonly Type TypeOfFloat = typeof(float);
    public static readonly Type TypeOfObject = typeof(object);
    public static readonly Type TypeOfSbyte = typeof(sbyte);
    public static readonly Type TypeOfDateTime = typeof(DateTime);
  }
}